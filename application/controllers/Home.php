<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		/// Load Models ///
		$this->load->model('home_model');
		/// End loading ///
		if (isset($_SESSION['lang'])) {
			$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
		} else {
			$data['lang'] = $this->home_model->get_langs('rus');
		}

		/// Load Arrays ///
		$data['top_charters'] = $this->home_model->get_top_charters(); 

		/// End Arrays ///



			/// Load View ///
		$this->load->view('header',$data);
	
		$this->load->view('home',$data);
		$this->load->view('footer',$data);
		/// End View ///
		 
	}
	public function lang($lang)
	{

		if (isset($lang)) {
			$_SESSION['lang'] = $lang;
			header("location: ".$_SERVER['HTTP_REFERER']);
		} else {
			$_SESSION['lang'] = 'rus';
			header("location: ".$_SERVER['HTTP_REFERER']);
		}
	}
}
