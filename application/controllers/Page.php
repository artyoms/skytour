<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function index()
	{
 
		/// Load Models ///
		$this->load->model('page_model');
		/// End loading ///


		/// Load Arrays ///
		$data['top_charters'] = $this->page_model->get_top_charters(); 

		/// End Arrays ///

	} 


	public function charter($id = NULL){

		/// Load Models ///
			$this->load->model('home_model');
			/// End loading ///

		if (isset($_SESSION['lang'])) {
				$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
			} else {
				$data['lang'] = $this->home_model->get_langs('rus');
			}


			$this->load->model('page_model');

			if (isset($_REQUEST['date_one']) == true and isset($_REQUEST['date_two']) == true and isset($_REQUEST['country']) == true) {
				$date_one =  date("Y-m-d", strtotime($_REQUEST['date_one']));
				$date_two =  date("Y-m-d", strtotime($_REQUEST['date_two']));
				$country = $_REQUEST['country'];

				$data['hotels'] = $this->page_model->get_hotels_search($date_one,$date_two,$country,$_REQUEST['cash']); 
				$data['country'] = $this->page_model->get_coutry();
	 			 
				$this->load->view('header',$data);
				$this->load->view('charter',$data);
				$this->load->view('footer',$data);


			} else {
			 
				if (isset($id)) {
					 
					  
					$data['hotels'] = $this->page_model->get_hotel_search($id); 
					$data['country'] = $this->page_model->get_coutry();
		 			 
					$this->load->view('header',$data);
					$this->load->view('charter',$data);
					$this->load->view('footer',$data);
				} else {
					$data['hotels'] = $this->page_model->get_hotels(); 
					$data['country'] = $this->page_model->get_coutry();
		 
					$this->load->view('header',$data);
					$this->load->view('charter',$data);
					$this->load->view('footer',$data);
				}
			}
	}
	public function transport($id = NULL){

		/// Load Models ///
			$this->load->model('home_model');
			/// End loading ///

		if (isset($_SESSION['lang'])) {
				$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
			} else {
				$data['lang'] = $this->home_model->get_langs('rus');
			}


			$this->load->model('page_model');
			
			$data['trans'] = $this->page_model->get_trans();


 			
			$this->load->view('header',$data);
 			$this->load->view('trans',$data);
			$this->load->view('footer',$data);
	}

	public function armenia($id){
				/// Load Models ///
			$this->load->model('home_model');
			/// End loading ///

		if (isset($_SESSION['lang'])) {
				$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
			} else {
				$data['lang'] = $this->home_model->get_langs('rus');
			}

			$this->load->model('page_model');

			$data['data'] = $this->page_model->get_armenia($id);

			$this->load->view('header',$data);
			$this->load->view('data',$data);
			$this->load->view('footer',$data);
	}


	public function tours(){
				/// Load Models ///
			$this->load->model('home_model');
			/// End loading ///

		if (isset($_SESSION['lang'])) {
			$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
		} else {
			$data['lang'] = $this->home_model->get_langs('rus');
		}


			$this->load->view('header',$data);
			$this->load->view('tours',$data);
			$this->load->view('footer',$data);
	}


	public function send(){
		/// Load Models ///
		$this->load->model('home_model');
		/// End loading ///
		if (isset($_SESSION['lang'])) {
			$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
		} else {
			$data['lang'] = $this->home_model->get_langs('rus');
		}

		if (isset($_REQUEST['mob'])) {
	
			if (isset($_REQUEST['desc'])) {
				$des = $_REQUEST['desc'];
			} else {
				$des = "NOT";
			}

			if (isset($_REQUEST['vilet'])) {
				$vilet = $_REQUEST['vilet']; 
			} else {
				$vilet = "Not select";
			}

			if (isset($_REQUEST['gothe'])) {
				$gothe = $_REQUEST['gothe'];
			} else {
				$gothe = 'Not select';
			}

			if (isset($_REQUEST['dateto'])) {
				$dateto = $_REQUEST['dateto'];
			} else {
				$dateto = 'Not select';
			}

			if (isset($_REQUEST['goback'])) {
				$goback = $_REQUEST['goback'];
			} else {
				$goback = '';
			}

			if (isset($_REQUEST['fio'])) {
				$fio = $_REQUEST['fio'];
			} else {
				$fio = '';
			}
			if (isset($_REQUEST['mob'])) {
				$mob = $_REQUEST['mob'];
			} else {
				$mob = '';
			}

			if (isset($_REQUEST['passag'])) {
				$passag = $_REQUEST['passag'];
			} else {
				$passag = '';
			}
			if (isset($_REQUEST['passag_min'])) {
				$passag_min = $_REQUEST['passag_min'];
			} else {
				$passag_min = '';
			}

			if (isset($_REQUEST['passages_min'])) {
				$passages_min = $_REQUEST['passages_min'];
			} else {
				$passages_min = '';
			}

			if (isset($_REQUEST['one_d'])) {
				$one_d = $_REQUEST['one_d'];
			} else {
				$one_d = '';
			}

			if (isset($_REQUEST['two_d'])) {
				$two_d = $_REQUEST['two_d'];
			} else {
				$two_d = '';
			}

			//загружаем библиотеку email
			 $this->load->library ('email');
			 
			 //выполняем инициализацию и заполняем все необходимые поля
			$this->email->initialize();
			$this->email->mailtype = 'html';
			$this->email->from('admin@skytour.am', 'skytour');
			//$this->email->to('');
			$this->email->to('skytour.am@yandex.ru');
			$this->email->subject('Новый заказ - Туры');
			$this->email->message('

				<center>
					Новый заказ в - '.date("d-m-Y h:m:s").'
				</center>
				<br>
				<table style="width:100%;"> 
					<tr> 
						<td>Город вылета: </td> <td>'.$vilet.'</td>
					</tr> 
					<tr> 
						<td>Город назначения: </td> <td>'.$gothe.'</td>
					</tr> 
					<tr> 
						<td>Туда: </td> <td>'.$dateto.'</td>
					</tr> 
					<tr> 
						<td>Обратно: </td> <td>'.$goback.'</td>
					</tr> 
					<tr> 
						<td>ФИО: </td> <td>'.$fio.'</td>
					</tr> 
					<tr> 
						<td>Номер телефона: </td> <td>'.$mob.'</td>
					</tr> 
 
					<tr> 
						<td>Взрослых: </td> <td>'.$passag.'</td>
					</tr> 
					<tr> 
						<td>Детей: </td> <td>'.$passag_min.'</td>
					</tr> 
					<tr> 
						<td>Младенцев: </td> <td>'.$passages_min.'</td>
					</tr> 
					<tr> 
						<td>Цена с : </td> <td>'.$one_d.'</td>
					</tr> 
					<tr> 
						<td>Цена до : </td> <td>'.$two_d.'</td>
					</tr>
					<tr>
						<td>Дополнительно: </td><td>'.$des.'</td>
					</tr>
				</table>

			');
			 
			if (!$this->email->send()) {
				header("location: /index.php/Page/armenia/25");
			} else {
				header("location: /index.php/Page/armenia/24");
			} 	
		} else {
			header("location: /index.php/Page/armenia/25");
		}


 
	}
 

	public function chart($count){
		$this->load->model('page_model');
		$data = $this->page_model->get_char($count);

	 

		foreach ($data as $row) {

		 
			echo "<li style='list-style-type: none;'><a href='/index.php/Page/charter/".$row->chid."' style='color: #ffffff;'>".$row->data_to." - ".$row->data_end." </a></li>";
				 
		}
		$_SESSION['ldi'] = $count;
	}

	public function send_chart(){
				/// Load Models ///
		$this->load->model('home_model');
		/// End loading ///
		if (isset($_SESSION['lang'])) {
			$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
		} else {
			$data['lang'] = $this->home_model->get_langs('rus');
		}


//		if (isset($_REQUEST['email']) == true or isset($_REQUEST['mob']) == true) {

			if (isset($_REQUEST['fio'])) {
				$fio = $_REQUEST['fio'];
			} else {
				$fio = 'Not select';
			}

			$this->email->initialize();
			$this->email->mailtype = 'html';
			$this->email->from('admin@skytour.am', 'skytour');
			$this->email->to('skytour.am@yandex.ru');
			//$this->email->to('manager@skytour.am');
			$this->email->subject('Новый заказ - Чартеры');
			$this->email->message('

				<center>
					Новый заказ в - '.date("d-m-Y h:m:s").'
				</center>
				<br>
				<table style="width:100%;"> 
					<tr>
						<td>Ссылка на чартер: '.$_SERVER['HTTP_REFERER'].'</td>

					</tr>
					
					<tr>
						<td>ФИО: </td><td>'.$fio.'</td>
					</tr>
					<tr>
						<td>Email: </td><td> '.$_REQUEST['email'].'</td>
					</tr>
					<tr>
						<td>Моб: </td><td>'.$_REQUEST['mob'].'</td>
					</tr>
					<tr>
						<td>Text: </td> <td>'.$_REQUEST['text'].'</td>
					</tr>

				</table>


			');
			if (!$this->email->send()) {
				header("location: /index.php/Page/armenia/25");
			} else {
				header("location: /index.php/Page/armenia/24");
			} 
	//	} else {
	//		header("location: /index.php/Page/armenia/25");
	//	}
	}











	public function send_car(){
				/// Load Models ///
		$this->load->model('home_model');
		/// End loading ///
		if (isset($_SESSION['lang'])) {
			$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
		} else {
			$data['lang'] = $this->home_model->get_langs('rus');
		}


		if (isset($_REQUEST['email']) == true or isset($_REQUEST['mob']) == true) {

			if (isset($_REQUEST['fio'])) {
				$fio = $_REQUEST['fio'];
			} else {
				$fio = 'Not select';
			}

			$this->email->initialize();
			$this->email->mailtype = 'html';
			$this->email->from('admin@skytour.am', 'skytour');
			$this->email->to('skytour.am@yandex.ru');
			//$this->email->to('manager@skytour.am');
			$this->email->subject('Новый заказ - Машины');
			$this->email->message('

				<center> 
					Новый заказ в - '.date("d-m-Y h:m:s").'
				</center>
				<br>
				<table style="width:100%;"> 
					<tr>
						<td>Машина : '.$_REQUEST['name'].'</td>

					</tr>
					
					<tr>
						<td>ФИО: </td><td>'.$fio.'</td>
					</tr>
					<tr>
						<td>Email: </td><td> '.$_REQUEST['email'].'</td>
					</tr>
					<tr>
						<td>Моб: </td><td>'.$_REQUEST['mob'].'</td>
					</tr>
					<tr>
						<td>Text: </td> <td>'.$_REQUEST['text'].'</td>
					</tr>

				</table>


			');
			if (!$this->email->send()) {
				header("location: /index.php/Page/armenia/25");
			} else {
				header("location: /index.php/Page/armenia/24");
			} 
		} else {
		//	header("location: /index.php/Page/armenia/25");
		}
	}









	public function send_armhotel(){
				/// Load Models ///
		$this->load->model('home_model');
		/// End loading ///

		if (isset($_SESSION['lang'])) {
			$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
		} else {
			$data['lang'] = $this->home_model->get_langs('rus');
		}


		if (isset($_REQUEST['email']) == true or isset($_REQUEST['mob']) == true) {

			if (isset($_REQUEST['fio'])) {
				$fio = $_REQUEST['fio'];
			} else {
				$fio = 'Not select';
			}

			$this->email->initialize();
			$this->email->mailtype = 'html';
			$this->email->from('admin@skytour.am', 'skytour');
			$this->email->to('skytour.am@yandex.ru');
			//$this->email->to('manager@skytour.am');
			$this->email->subject('Новый заказ - Arm Hotels');
			$this->email->message('

				<center>
					Новый заказ в - '.date("d-m-Y h:m:s").'
				</center>
				<br>
				<table style="width:100%;"> 
					<tr>
						<td>Ссылка на чартер: '.$_SERVER['HTTP_REFERER'].'</td>

					</tr>
					
					<tr>
						<td>ФИО: </td><td>'.$fio.'</td>
					</tr>
					<tr>
						<td>Email: </td><td> '.$_REQUEST['email'].'</td>
					</tr>
					<tr>
						<td>Моб: </td><td>'.$_REQUEST['mob'].'</td>
					</tr>
					<tr>
						<td>Text: </td> <td>'.$_REQUEST['text'].'</td>
					</tr>

				</table>


			');
			if (!$this->email->send()) {
				header("location: /index.php/Page/armenia/25");
			} else {
				header("location: /index.php/Page/armenia/24");
			} 
		} else {
			header("location: /index.php/Page/armenia/25");
		}
	}


	public function hotel($id){
				/// Load Models ///
		$this->load->model('home_model');
		/// End loading ///

		if (isset($_SESSION['lang'])) {
			$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
		} else {
			$data['lang'] = $this->home_model->get_langs('rus');
		}


		$this->load->model('page_model');

			$data['data'] = $this->page_model->get_hotel($id);

			$this->load->view('header',$data);
			$this->load->view('hotel',$data);
			$this->load->view('footer',$data);
	}

	public function armhotel() {
		if (isset($_REQUEST['country'])) {
		
		$this->load->model('home_model');

		if (isset($_SESSION['lang'])) {
			$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
		} else {
			$data['lang'] = $this->home_model->get_langs('rus');
		}

		$this->load->model('page_model');
		$data['data'] = $this->page_model->get_armHotelsRand($_REQUEST['country']);
		$data['country'] = $this->page_model->get_armhote_county();
			$this->load->view('header',$data);
			$this->load->view('armhotel',$data);
			$this->load->view('footer',$data);

		} else {

		$this->load->model('home_model');
		if (isset($_SESSION['lang'])) {
				$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
			} else {
				$data['lang'] = $this->home_model->get_langs('rus');
			}
		$this->load->model('page_model');
		$data['data'] = $this->page_model->get_armHotelsRand();
		$data['country'] = $this->page_model->get_armhote_county();
			$this->load->view('header',$data);
			$this->load->view('armhotel',$data);
			$this->load->view('footer',$data);
		}
	}

	public function carRent($id = 0){

	}


	public function armfullhotel($id){
			/// Load Models ///
		$this->load->model('home_model');
		/// End loading ///

		if (isset($_SESSION['lang'])) {
			$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
		} else {
			$data['lang'] = $this->home_model->get_langs('rus');
		}


			$this->load->model('page_model');

			$data['data'] = $this->page_model->get_armhotel($id);
 
			$this->load->view('header',$data);
			$this->load->view('armfull',$data);
			$this->load->view('footer',$data);
	}


	public function contacts(){
	 		/// Load Models ///
		$this->load->model('home_model');
		/// End loading ///
			if (isset($_SESSION['lang'])) {
				$data['lang'] = $this->home_model->get_langs($_SESSION['lang']);
			} else {
				$data['lang'] = $this->home_model->get_langs('rus');
			}


			$this->load->view('header',$data);
			$this->load->view('contacts',$data);
			$this->load->view('footer',$data);
	}

}
 