<?php
 
    if(isset($_SESSION['lang'])) {
        
    } else {
        $_SESSION['lang'] = 'rus';
    } 

?> 
 <!-- Section Title-->    
            <div class="section-title-01" style="width: 1230px;   height: 300px; margin: 0 auto;">
                <!-- Parallax Background -->
                <div class="bg_parallax image_02_parallax" style="background:url('/uploads/Glxavor%20SLide/byron.jpg')"></div>
                <!-- Parallax Background -->

                <!-- Content Parallax-->
                <div class="opacy_bg_02">
                     <div class="container">
                  
                             <!-- Section Title-->
                                    <div class="title-detailed" style="bottom: 0px;">
                                       
                                                <div class="col-xs-3 row">
                                                   
                                                    <h2 style="font-size:150%;   float:left;    text-align: initial;">
                                                <?php

                                                    foreach ($data as $key) {
                                                        if ($_SESSION['lang'] == 'rus') {
                                                           echo $key->name_rus;  
                                                        } else {

                                                            echo $key->name_eng;
                                                        }
                                                              
                                                    }

                                                ?>
                                                    </h2>
                                               
                                        </div>
                                    </div>
                    </div>  
                </div>  
                <!-- End Content Parallax--> 
            </div>   
            <!-- End Section Title-->

            <!--Content Central -->
            <section class="content-central" style='margin-top: 0px;'>
                <!-- Shadow Semiboxed -->
                
                <!-- End Shadow Semiboxed -->

                <!-- End content info - page Fill with -->
                <div class="content_info">
                    <div class="paddings-mini">
                        <div class="container">
                           
                            <?php
                 
                                foreach ($data as $key) {
                                      
                                       if ($_SESSION['lang'] == 'rus') {
                                        echo $key->description_rus; 
                                    } else {

                                       echo $key->description_eng;
                                    }
                                         
                                }

                            ?>

                        </div>
                    </div>
                </div>   
                <!-- End content info - page Fill with  --> 
 
            </section>
            <!-- End Content Central -->