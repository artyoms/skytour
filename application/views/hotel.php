<?php
 
    if(isset($_SESSION['lang'])) {
        
    } else {
        $_SESSION['lang'] = 'rus';
    } 

?>  <!-- Section Title-->    
            <div  class="section-title-detailed"  style="height: 460px;width: 1230px; margin: 0 auto;" >
                <!-- Single Carousel-->
                <div id="single-carousel">
                    <div class="img-hover">
                        <div class="overlay"> <a href="<?php
                                             foreach ($data as $key) {
                                                echo $key->images;
                                             }
                                             ?>" class="fancybox" rel="gallery"></a></div>
                        <img src="<?php
                                             foreach ($data as $key) {
                                                echo $key->images;
                                             }
                                             ?>" alt="" class="img-responsive">
                    </div>

                     
                </div>
                <!-- End Single Carousel-->

                <!-- Section Title-->
                <div class="title-detailed" style="    bottom: 0px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9">
                                <ul class="starts">
                                   <?php    

                                            foreach ($data as $key) {
                                            
                                                for ($i=0; $i < $key->rank; $i++) { 
                                                    echo('<li><a href="#"><i class="fa fa-star"></i></a></li>');
                                                }

                                            }


                                    ?>

                                  
                                </ul>
                                <h2><?php
                                    foreach ($data as $key) {
                                        echo $key->title;
                                    }
                                ?> 
                                    <span><?php

                                            foreach ($data as $key) {
                                                echo "C ".$key->data_to." до ".$key->data_end;
                                            }
                                    ?></span>
                                </h2>
                            </div>

                            <div class="col-md-3">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Section Title-->
            </div>   
            <!-- End Section Title-->

            <!--Content Central -->
            <div class="content-central">
                <!-- Shadow Semiboxed -->
                <div class="semiboxshadow text-center">
                    <img src="img/img-theme/shp.png" class="img-responsive" alt="">
                </div>
                <!-- End Shadow Semiboxed -->

                <!-- End content info - Features-->
                <div class="content_info skin_base no-overflow">
                    <div class="container wow fadeInUp">
                        <div class="row">
                            <!-- Services Items -->
                            <div class="col-md-9">
                                    <p style="color:#000;font-size:16px;">
                                        <br>
                                        <?php

                                            foreach ($data as $key) {
                                                echo $key->description_rus;
                                            }

                                        ?>
                                    </p>
                            </div> 
                            <!-- End Services Items --> 

                            <!-- Form Detailed -->

                            <div class="col-md-3">
                                <div class="form-detailed" style="    margin-top: 0px;">
                                    <div class="header-detailed">
                                        <div class="price-detailed" style="width:100%;">
                                            <?php
                                             foreach ($data as $key) {
                                                if ($key->dbl == 2) {
                                                    $dbl = "DBL";
                                                } else {
                                                    $dbl = "";
                                                }

                                                switch ($key->kurs) {
                                                    case 'USD':
                                                       $d  = "$";
                                                    break;
                                                    
                                                    case 'EUR':
                                                       $d  = "€";
                                                    break;
  
                                                    case 'AMD':
                                                       $d  = "AMD";
                                                    break;
                                                    
                                                    default:
                                                        # code...
                                                        break;
                                                }

                                                echo $key->cash.$d." "." (".$dbl.")";

                                             }
                                            ?>
                                        </div>

                                        <div class="frequency-detailed">
                                            
                                        </div>
                                    </div>
                                    
                                    <form onsubmit="return form_check()" action="/index.php/Page/send_chart" method='post'>
                                        <label>И.Ф.О</label>
                                        <input type="text" required="required" style="width:100%;" name='fio' placeholder="Имя Фамилие Отчество" >
                                        <label>Email</label>
                                        <input type="text" style="width:100%;" name='email' placeholder="Email">
                                        <label>Тел. номер</label>
                                        <input type="text" required="required" style="width:100%;"   name='mob' placeholder="Номер телефона" >
                                        <br><br>
                                            <textarea placeholder='Text' name='text' style="width:100%;"></textarea>
                                        <br><br>
                                        <input type="submit" value="Отправить">
                                    </form>
                                </div>
                            </div>   
                            <!-- End Form Detailed --> 
                        </div>
                    </div>
                </div>   
                <!-- End content info - Features--> 

                <!-- End content info - Grey Section-->
                <div class="content_info">
                    <!-- Info Resalt-->
                    <div class="content_resalt paddings-mini tabs-detailed">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Nav Tabs-->
                                    <ul class="nav nav-tabs" id="myTab">
                                     
                                       <li class="active">
                                            <a href="#faq" data-toggle="tab"><i class="fa fa-check"></i>Gallery</a>
                                        </li>
                                    </ul>
                                    <!-- End Nav Tabs-->

                                    <div class="tab-content">
                                      
                                        <!-- Tab Theree - faq -->
                                        <div class="tab-pane active" id="faq">
                                            <?php
                                             foreach ($data as $key) {
                                                echo $key->gallery;
                                             }
                                            ?>
                                        </div>
                                        <!-- Tend ab Theree - faq -->
                                    </div>  
                                </div>                 
                            </div>
                        </div>
                    </div>
                    <!-- End Info Resalt-->
                </div>   
                <!-- End content info - Grey Section--> 

                <!-- content info - testimonials-->
            
                <!-- End content info - testimonials-->
            </div>
            <!-- End Content Central -->
      