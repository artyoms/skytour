<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
 
     <?php
 
    if(isset($_SESSION['lang'])) {
        
    } else {
        $_SESSION['lang'] = 'rus';
    } 

?>        

            <!-- Section Title-->    
            <div class="section-title-01"  style="width: 1230px;   height:300px; margin: 0 auto;">
                <!-- Parallax Background -->
                <div class="bg_parallax image_04_parallax" style="background:url('/uploads/Glxavor%20SLide/byron.jpg');"></div>
                <!-- Parallax Background -->
 
                <!-- Content Parallax-->
                <div class="opacy_bg_02">
                     <div class="container">
                        <h1><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[100]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[100]->rus;
                                    }

                                 ?></h1>
                        <div class="crumbs">
                            <ul>
                                <li><a href="/"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[1]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[1]->rus;
                                    }

                                 ?></a></li>
                                <li>/</li>
                                <li><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[21]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[21]->rus;
                                    }

                                 ?></li>   
                            </ul>    
                        </div>
                    </div>  
                </div>  
                <!-- End Content Parallax--> 
            </div>   
            <!-- End Section Title-->

            <!--Content Central -->
            <div class="content-central">
                <!-- Shadow Semiboxed -->
             
                <!-- End Shadow Semiboxed -->

                <!-- End content info - page Fill with -->
                
                <div class="content_info">
                    <div class="container">
                        <div class="row">
                            <!-- Left Sidebar-->
                                
                            <div class="col-md-3">
                                <div class="container-by-widget-filter bg-dark color-white">
                                    <!-- Widget Filter -->
                                    <aside class="widget padding-top-mini">
                                        <h3 class="title-widget"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[111]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[111]->rus;
                                    }

                                 ?></h3>

                                        <!-- FILTER widget-->
                                        <div class="filter-widget">
                                         
                                                
                                                <select  class="form-control" id="getcount" name='country' required="required" placeholder='Страна' >
                                             
                                                    <?php

                                                        if (isset($_REQUEST['country'])) {
                                                            foreach ($country as $key) {
                                                                if ($_SESSION['lang'] == 'rus') {
                                                                    $l = $key->name_rus;
                                                                } else {
                                                                    $l = $key->name_eng;
                                                                }
                                                                if ($key->countid == $_REQUEST['country']) {
                                                                    echo "<option value='".$key->countid."'>".$l."</option>";
                                                                }
                                                            }
                                                        }

                                                    ?>

  

                                                    <?


                                                        foreach ($country as $key) {
                                                            if ($_SESSION['lang'] == 'rus') {
                                                                $l = $key->name_rus;
                                                            } else {
                                                                $l = $key->name_eng;
                                                            }

                                                            if (isset($_SESSION['ldi'])) {
                                                                echo "<option value='".$key->countid."'>".$l."</option>";
                                                                 ?>
                                                                   <option placeholder='Страна' value='9999'><?php

                                                                        if (isset($_SESSION['lang'])) {
                                                                            echo $lang[111]->$_SESSION['lang'];
                                                                        } else {
                                                                            echo $lang[111]->rus;
                                                                        }

                                                                     ?>
                                                                    </option>
                                                                 <?
                                                            } else {
                                                                ?>
                                                                   <option placeholder='Страна' value='9999'><?php

                                                                        if (isset($_SESSION['lang'])) {
                                                                            echo $lang[111]->$_SESSION['lang'];
                                                                        } else {
                                                                            echo $lang[111]->rus;
                                                                        }

                                                                     ?>
                                                                    </option>
                                                                <?
                                                                echo "<option value='".$key->countid."'>".$l."</option>";
                                                            }


                                                        }

                                                    ?>
                                                </select>
                                                <br>
                                                <center> 
                                                <ul style="padding: 0px;" class="counts">

                                                </ul></center>
                                                <script type="text/javascript">
                                                    $('#getcount').on('change', function() {
                                                      alert( this.value ); // or $(this).val()
                                                    });
                                                </script>
                                                <br>
                                             <!--   <input type="text" name='date_one' required="required" placeholder="<?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[32]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[32]->rus;
                                    }

                                 ?>" class="date-input">
                                                <input type="text" name='date_two' required="required" placeholder="<?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[33]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[33]->rus;
                                    }

                                 ?>" class="date-input">
                                                 
                                                <input type="submit" value="<?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[19]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[19]->rus;
                                    }

                                 ?>"> -->


                                         
                                        </div>
                                        <!-- END FILTER widget-->
                                    </aside>
                                    <!-- End Widget Filter -->

                                    <!-- Widget Filter Price Range-->
                                    
                                     
                                    <!-- End Widget Filter Price Range-->

                                    
                                      
                                </div>

                            </div>
                            <!-- End Left Sidebar-->
                           
<!--                            <div class="col-md-9">-->
<!--                                <!-- Title Results-->
<!--
<!--                                <!-- End Title Results-->
<!--                                -->
<!--                                -->
<!--                                <!-- sort-by-container-->
<!--                                <style type="text/css">-->
<!---->
<!--                                    .price span {-->
<!--                                        color: #000;-->
<!--                                    }-->
<!--                                </style>-->
<!--                                <div class="row list-view">-->
<!--                             -->
<!--                                    <br>-->
<!--                                    --><?php //foreach ($hotels as $key) {
//
//
//                                    ?>
<!---->
<!---->
<!--                                    <div class="col-md-12" style="display: table-cell;">-->
<!--                                    <a href="/index.php/Page/hotel/--><?php //echo $key->id; ?><!--"  >-->
<!--                                        <div class="img-hovers">-->
<!--                                        <img src="--><?php //echo $key->images; ?><!--" alt="" class="img-responsive">-->
<!--                                        <div class="overlay"><a href="--><?php //echo $key->images; ?><!--" class="fancybox"> </a></div>-->
<!--                                        </div>-->
<!--                                    </a>-->
<!--                                        <div class="info-gallery">-->
<!--                                            <h3> -->
<!--                                                <a href="/index.php/Page/hotel/--><?php //echo $key->id; ?><!--"  > <span><b style='    font-size: 150%;'>-->
<!--                                                    --><?php
//
//                                                      echo  $key->title."<span style='font-size:60%;'><br> (С ".$key->data_to." до ".$key->data_end.")</span>";
//
//
//                                                    ?>
<!---->
<!--                                                </b></span></a>-->
<!--                                            </h3>-->
<!---->
<!--                                        -->
<!---->
<!--                                           -->
<!--                                            <div class="price" style="    float: right;left:309px;position:initial;"><small style="font-size:30%;color:#000;">DBL </small><small style="color:#000;font-size:60%;">  --><?php //echo $key->cash; ?><!--</small><span style="color:000;font-size:30%;"> --><?php //
//                                             switch ($key->kurs) {
//                                                    case 'USD':
//                                                       $d  = "$";
//                                                    break;
//
//                                                    case 'EUR':
//                                                       $d  = "€";
//                                                    break;
//
//                                                    case 'AMD':
//                                                       $d  = "AMD";
//                                                    break;
//
//                                                    default:
//                                                          $d  = "$";
//                                                    break;
//                                                }
//                                                echo $key->kurs;
//                                            ?><!--</span></div>-->
<!---->
<!--                                            <hr class="separator">-->
<!--                                            --><?//
//                                                switch ($_SESSION['lang']) {
//                                                    case 'rus':
//                                                        $l = $key->description_rus;
//                                                        break;
//                                                    case 'eng':
//                                                          $l = $key->description_eng;
//                                                        break;
//                                                    case 'arm':
//                                                         $l = $key->description_arm;
//                                                        break;
//
//                                                }
//                                            ?>
<!--                                            <p>--><?php //  echo mb_substr($l,0, 300)."..."; ?><!--</p>-->
<!--                                            <ul class="starts">-->
<!--                                              -->
<!---->
<!--                                                --><?php
//
//                                                    for ($i=0; $i < $key->rank; $i++) {
//                                                        echo ' <li><a href="#"><i class="fa fa-star"></i></a></li>';
//
//                                                    }
//
//
//                                                ?><!-- -->
<!--                                            </ul>-->
<!--                                            <div class="content-btn"><a href="/index.php/Page/hotel/--><?php //echo $key->id; ?><!--" class="btn btn-primary">  --><?php
//
//                                                           if ($_SESSION['lang'] == 'eng') {
//                                                                echo $lang[78]->eng;
//                                                            } else {
//                                                                echo $lang[78]->rus;
//                                                            }
//
//                                                ?><!--</a></div>-->
<!--                                        -->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    -->
<!--                                    --><?php
//                                    } ?>
<!---->
<!--                                        <style type="text/css">-->
<!--                                            /* Galley list-view---------------------------------------------------------------*/-->
<!--                                            .list-view .img-hovers{-->
<!--                                                width: 40%;-->
<!--                                                float: left;-->
<!--                                            }-->
<!--                                            .list-view .info-gallery{-->
<!--                                                padding: 23px 20px 27px 20px;-->
<!--                                                width: 60%;-->
<!--                                                text-align: left;-->
<!--                                                float: right;-->
<!--                                            }-->
<!--                                            .list-view .info-gallery:before {-->
<!--                                                border: 20px solid #fff;-->
<!--                                                border-left-color: transparent;-->
<!--                                                border-top-color: transparent;-->
<!--                                                border-bottom-color: transparent;-->
<!--                                                content: "";-->
<!--                                                display: block;-->
<!--                                                top: 30px;-->
<!--                                                left: -40px;-->
<!--                                                position: absolute;-->
<!--                                                z-index: 999;-->
<!--                                            }-->
<!--                                            .list-view .info-gallery .separator{-->
<!--                                                margin: 2px 0 15px 0 !important;-->
<!--                                                max-width: 100%;-->
<!--                                            }-->
<!--                                            .list-view .info-gallery  hr.separator{-->
<!--                                                width: 100%;-->
<!--                                            }-->
<!--                                            .list-view .info-gallery .price{-->
<!--                                                float: left;-->
<!--                                                position: absolute;-->
<!--                                                top: 21px;-->
<!--                                                left: 383px;-->
<!--                                                text-shadow: 2px 3px 3px rgba(0,0,0,0.5);-->
<!--                                                font-size: 2.5rem;-->
<!--                                                line-height: 2rem;-->
<!--                                                font-weight: normal;-->
<!--                                                color: #000;-->
<!--                                            }-->
<!--                                            .list-view .info-gallery p {-->
<!--                                              margin-bottom: 20px;-->
<!--                                            }-->
<!--                                            .list-view .info-gallery .btn {-->
<!--                                              float: left;-->
<!--                                            }-->
<!--                                            .list-view .info-gallery .starts {-->
<!--                                              float: right;-->
<!--                                              margin-bottom: 0;-->
<!--                                            }-->
<!--                                        </style>-->
<!--                                  -->
<!--                                    <!-- End pagination-->               
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                </div>   
                <!-- End content info - page Fill with  --> 
            </div>
            <!-- End Content Central -->
   