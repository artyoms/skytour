<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php
 
    if(isset($_SESSION['lang'])) {
        
    } else {
        $_SESSION['lang'] = 'rus';
    } 

?> 
            

            <!-- Section Title-->    
            <div class="section-title-01"  style="height: 460px;" >
                <!-- Parallax Background -->
                <div class="bg_parallax image_04_parallax"></div>
                <!-- Parallax Background -->
 
                <!-- Content Parallax-->
                <div class="opacy_bg_02">
                     <div class="container">
                        <h1><?php

                                    if ($_SESSION['lang'] == 'eng') {
                                        echo $lang[103]->eng;
                                    } else {
                                        echo $lang[103]->rus;
                                    }

                                 ?></h1>
                        
                    </div>  
                </div>  
                <!-- End Content Parallax--> 
            </div>   
            <!-- End Section Title-->

            <!--Content Central -->
            <div class="content-central">
                <!-- Shadow Semiboxed -->
             
                <!-- End Shadow Semiboxed -->

                <!-- End content info - page Fill with -->
                
                <div class="content_info">
                    <div class="container">
                        <div class="row">
                            <!-- Left Sidebar-->
                               <form action="" method='post'>
                            <div class="col-md-3">
                                <div class="container-by-widget-filter bg-dark color-white">
                                    <!-- Widget Filter -->
                                    <aside class="widget padding-top-mini">
                                        <h3 class="title-widget"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[112]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[112]->rus;
                                    }

                                 ?></h3>

                                        <!-- FILTER widget-->
                                        <div class="filter-widget">
                                         
                                                
                                                <select  class="form-control" name='country' id="country" required="required" placeholder='Страна' >
 
                                                    <?php

                                                        if (isset($_REQUEST['country'])) {
                                                            foreach ($country as $key) {
                                                                if ($_SESSION['lang'] == 'rus') {
                                                                    $l = $key->name_rus;
                                                                } else {
                                                                    $l = $key->name_eng;
                                                                }
                                                                if ($key->catid == $_REQUEST['country']) {
                                                                    
                                                                    echo "<option value='".$key->catid."'>".$l."</option>";
                                                                }
                                                            }
                                                        }

                                                    ?>

                                                    <option placeholder='Страна' value='9999'><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[112]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[112]->rus;
                                    }

                                 ?></option>
                                                    <?php

                                                        foreach ($country as $key) {
                                                            if ($_SESSION['lang'] == 'rus') {
                                                                $l = $key->name_rus;
                                                            } else {
                                                                $l = $key->name_eng;
                                                            }

                                                            echo "<option value='".$key->catid."'>".$l."</option>";
                                                        }

                                                    ?>
                                                </select>
                                                <br>
                                                
                                                <br>
                                             <!--   <input type="text" name='date_one' required="required" placeholder="<?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[32]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[32]->rus;
                                    }

                                 ?>" class="date-input">
                                                <input type="text" name='date_two' required="required" placeholder="<?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[33]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[33]->rus;
                                    }

                                 ?>" class="date-input">
                                                 
                                                <input type="submit" value="<?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[19]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[19]->rus;
                                    }

                                 ?>"> -->
                                           
                                         
                                 <input type="submit" value="<?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[19]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[19]->rus;
                                    }

                                 ?>">
                         
                                        </div>
                                        <!-- END FILTER widget-->
                                    </aside>
                                    <!-- End Widget Filter -->

                                    <!-- Widget Filter Price Range-->
                                   
                                    <!-- End Widget Filter Price Range-->
  
                                    
                                      
                                </div>

                            </div>
                            <!-- End Left Sidebar-->

                            </form>
                            <div class="col-md-9">
                                <!-- Title Results-->
                              
                                <!-- End Title Results-->
                                
                                
                                <!-- sort-by-container-->
                                <style type="text/css">

                                    .price span {
                                        color: #000;
                                    }
                                </style>
                                <div class="row list-view">
                                    <!-- Item Gallery List View-->
                                    <br>
                                    <?php foreach ($data as $key) {
                                    	if ($key->image != '') {
                                    	 

                                    ?>


                                    <div class="col-md-12" style="display: inline-block;">
                                       <a href="/index.php/Page/armfullhotel/<?php echo $key->id; ?>">
                                        <div class="img-hovers">
                                            <img src="<?php echo $key->image; ?>" alt="" class="img-responsive">
                                            <div class="overlay"><a href="<?php echo $key->image; ?>" class="fancybox"> </a></div>
                                        </div>
                                    </a>
                                        <div class="info-gallery">
                                            <h3> 
                                                 <a href="/index.php/Page/armfullhotel/<?php echo $key->id; ?>">
                                                    <span><b style="font-size: 135%;">
                                                <?php
                                                    if (isset($_SESSION['lang'])) {
                                                        if ($_SESSION['lang'] == 'rus') {
                                                            echo  $key->title_rus;
                                                        } else {
                                                            echo  $key->title_eng;
                                                        }
                                                    }
                                               ?>

                                                </b></span>
                                            </a>
                                            </h3>
                                               <div class="price" style="left:309px;position:initial;"><small style="font-size:30%;color:#000;">DBL </small><small style="color:#000;font-size:60%;"><?php echo $key->cash; ?></small><span style="color:000;font-size:30%;" > AMD</span></div>
                                       
                                            <hr class="separator">
                                            <p><?php echo mb_substr($key->desctiption_rus,0, 215)."..."; ?></p>
                                            <ul class="starts">
                                              

                                                <?php

                                                    for ($i=0; $i < $key->rank; $i++) { 
                                                        echo ' <li><a href="#"><i class="fa fa-star"></i></a></li>';
                                                       
                                                    }


                                                ?> 
                                            </ul>
                                            <div class="content-btn"><a href="/index.php/Page/armfullhotel/<?php echo $key->id; ?>" class="btn btn-primary">
                                                <?php

                                                           if ($_SESSION['lang'] == 'eng') {
                                                                echo $lang[78]->eng;
                                                            } else {
                                                                echo $lang[78]->rus;
                                                            }

                                                ?>
                                            </a></div>
                                          
                                        </div>
                                    </div>
                                    <!-- End Item Gallery List View-->
                                    <?php
                                    	# code...
                                    	}
                                    } ?>

                                        
                                  
                                    <!-- End pagination-->               
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
                <!-- End content info - page Fill with  --> 
            </div>
            <!-- End Content Central -->


            <style type="text/css">

                .list-view .img-hovers{
                    width: 40%;
                    float: left;
                }
                .list-view .info-gallery{
                    padding: 23px 20px 27px 20px;
                    width: 60%;
                    text-align: left;
                    float: right;
                }
                .list-view .info-gallery:before {
                    border: 20px solid #fff;
                    border-left-color: transparent;
                    border-top-color: transparent;
                    border-bottom-color: transparent;
                    content: "";
                    display: block;
                    top: 30px;
                    left: -40px;
                    position: absolute;
                    z-index: 999;
                }
                .list-view .info-gallery .separator{
                    margin: 2px 0 15px 0 !important;
                    max-width: 100%;
                }
                .list-view .info-gallery  hr.separator{
                    width: 100%;
                }
                .list-view .info-gallery .price{
                    float: right;
                    position: absolute;
                    top: 30px;
                    left: -190px;
                    text-shadow: 2px 3px 3px rgba(0,0,0,0.5);
                    font-size: 2.5rem;
                    line-height: 2rem;
                    font-weight: normal;
                    color: #fff;
                }
                .list-view .info-gallery p {
                  margin-bottom: 20px;
                }
                .list-view .info-gallery .btn {
                  float: left;
                }
                .list-view .info-gallery .starts {
                  float: right;
                  margin-bottom: 0;
                }

            </style>
   