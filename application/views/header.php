<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <title>SkyTour</title>
      

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <!-- Theme CSS -->
        <link href="/template/css/style.css" rel="stylesheet" media="screen">
        <!-- Responsive CSS -->
        <link href="/template/css/theme-responsive.css" rel="stylesheet" media="screen">
        <!-- Skins Theme -->
        <link href="#" rel="stylesheet" media="screen" class="skin">
        <link rel="stylesheet" type="text/css" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
        <!-- Favicons -->
        <link rel="shortcut icon" href="/template/img/icons/logo%20(2).ico">
        <link rel="apple-touch-icon" href="/template/img/logo/logo.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/template/img/logo/logo.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/template/img/logo/logo.png">  
        <link rel="stylesheet" type="text/css" href="/css/skins/blue/blue.css">
        <!-- Head Libs -->
        <script src="/template/js/modernizr.js"></script>
         
        <!--[if IE]>
            <link rel="stylesheet" href="css/ie/ie.css">
        <![endif]-->

        <!--[if lte IE 8]>
            <script src="js/responsive/html5shiv.js"></script>
            <script src="js/responsive/respond.js"></script>
        <![endif]-->
    </head>
    <body>
       
        <!-- layout-->
        <div id="layout">
<!-- Header-->
            <header id="header" class="header-v1">
                <!-- Main Nav -->
                <nav class="flat-mega-menu">            
                    <!-- flat-mega-menu class -->
                    <label for="mobile-button" style="  width: 125px;"> Menu <i class="fa fa-bars"></i></label><!-- mobile click button to show menu -->
                    <input id="mobile-button" type="checkbox">                          

                    <ul class="collapse"><!-- collapse class for collapse the drop down -->
                        <!-- website title - Logo class -->
                        <li class="title">
                            <a href="/"><img src="/template/img/logo/logo.png" style="width: auto;  height: 50px;  margin-top: 4px;  border: none;  background: none;  box-shadow: none;"></a> 
                            
                        </li>
                        <!-- End website title - Logo class -->

                      
                        <li>
                            <a href="/">
                                <?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[1]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[1]->rus;
                                    }

                                 ?>
                             
                            </a>
                        </li>
                        
                        

                        <li>
                            <a href="/index.php/Page/charter">  <?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[21]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[21]->rus;
                                    }

                                 ?></a>
                         
                        </li>

                        <li> <a href="/index.php/Page/armenia/27">
                            <?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[4]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[4]->rus;
                                    }

                                 ?></a>
                        </a> 
                          
                        </li>


                        <li> <a href="/index.php/Page/armenia/26"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[79]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[79]->rus;
                                    }

                                 ?></a>
                           
                        </li>

                        <li> <a href="/index.php/Page/armenia/22"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[3]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[3]->rus;
                                    }

                                 ?></a> 
                             
                        </li>

                        

                      
                        <li>
                            <a href="/index.php/Page/contacts"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[2]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[2]->rus;
                                    }

                                 ?></a>
                        </li>
                
                        <li class="login-form" style="margin-left: 0px;padding-left: 10px; min-height:0px;border-left: 0px solid #dedede;padding-right: 10px;">
                          	<div style=" width: 100px;    margin: 0 auto;">
								
							 
							<a href="/index.php/Home/lang/eng" style="padding-left: 0px;padding-right: 0px;">
                                <img src="http://avtostankoprom.ru/wp-content/themes/avtostankoprom-tpl/img/flag-eng.jpg" style="width:20px;margin-top:22px;">
                            </a>
                            <a href="/index.php/Home/lang/rus" style="padding-left: 0px;padding-right: 0px;  padding-right: 10px">
                                <img src="http://www.daviscup.com/itf/flags/46/RUS.gif" style="    margin-left: 8px;width:20px;margin-top:20px;">
                            </a>
                            <a href="/index.php/Home/lang/arm" style="padding-left: 0px;padding-right: 0px;  padding-right: 10px">
                                <img src="http://www.fca.am/borrowwisely/img/arm_flag.jpg" style="    margin-left: 8px;width:20px;margin-top:21px;">
                            </a>
								</div>
                        </li>    
         
					<style>
						@media screen and (max-width: 999px){
.flat-mega-menu > label {
    width: 60px;
    height: 60px;
    display: block;
    margin: 0;
    padding: 0;
    position: absolute;
    top: 0;
    right: 0;
    z-index: 99;
    text-align: center;
    line-height: 60px;
    font-size: 2rem;
    color: #000000;
    cursor: pointer;
}
							}
						@media handheld, only screen and (max-width: 999px){
							.header-v1 {
								background: #fff;
								max-height: 2000px !important;
							}
						}
				
				
				</style>
            	<!--	<li class="login-form" style="margin-left: 0px;">
                             <a href="/index.php/Home/lang/arm" style="padding-left: 0px;padding-right: 0px;  padding-right: 10px">
                                <img src="http://www.daviscup.com/itf/flags/46/ARM.gif" style="width:30px;margin-top:20px;">
                             </a>
                        </li> -->

                    </ul>
                </nav>
                <!-- Main Nav -->
            </header>