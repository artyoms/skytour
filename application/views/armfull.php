<?php
 
    if(isset($_SESSION['lang'])) {
        
    } else {
        $_SESSION['lang'] = 'rus';
    } 

?>
 <!-- Section Title-->    
            <div class="section-title-detailed" style="height: 460px;width: 1230px; margin: 0 auto;">
                <!-- Single Carousel-->
                <div id="single-carousel">
                    <div class="img-hover">
                        <div class="overlay">  </div>
                        <img style="width:100%;" src="<?php
                                             foreach ($data as $key) {
                                                echo $key->image;
                                             }
                                             ?>" alt="" class="img-responsive">
                    </div>

                     
                </div>
                <!-- End Single Carousel-->

                <!-- Section Title-->
                <div class="title-detailed" style="    bottom: 0px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9">
                                <ul class="starts">
                                   <?php    

                                            foreach ($data as $key) {
                                            
                                                for ($i=0; $i < $key->rank; $i++) { 
                                                    echo('<li><a href="#"><i class="fa fa-star"></i></a></li>');
                                                }

                                            }


                                    ?>

                                  
                                </ul>
                                <h2 style="font-size:130%;"><?php
                                    foreach ($data as $key) {
                                     
                                                if ($_SESSION['lang'] == 'eng') {
                                                        echo $key->title_eng;
                                                } else {
                                                        echo $key->title_rus;
                                                }
                                    }
                                ?> 
                                    <span><?php

                                             
                                    ?></span>
                                </h2>
                            </div>

                            <div class="col-md-3">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Section Title-->
            </div>   
            <!-- End Section Title-->

            <!--Content Central -->
            <div class="content-central" style="margin-top: 0px;">
                <!-- Shadow Semiboxed -->
                <div class="semiboxshadow text-center">
                    <img src="img/img-theme/shp.png" class="img-responsive" alt="">
                </div>
                <!-- End Shadow Semiboxed -->

                <!-- End content info - Features-->
                <div class="content_info skin_base no-overflow">
                    <div class="container wow fadeInUp">
                        <div class="row">
                            <!-- Services Items -->
                            <div class="col-md-9">
                                    <p style="color:#000;font-size:16px;">
                                        <br>
                                        <?php

                                            foreach ($data as $key) {
                                               
                                                            if ($_SESSION['lang'] == 'rus') {
                                                                  echo $key->desctiption_rus;
                                                            } else {
                                                                  echo $key->desctiption_eng;
                                                            }
                                            }

                                        ?>
                                    </p>
                            </div> 
                            <!-- End Services Items --> 

                            <!-- Form Detailed -->
                            <div class="col-md-3">
                                <div class="form-detailed">
                                    <div class="header-detailed" style="background:none;">
                                        

                                        <div class="frequency-detailed">
                                            
                                        </div>
                                    </div>
                                    
                                    <form onsubmit="return form_check()" action="/index.php/Page/send_armhotel" method='post'>
                                        <label><?php 
                                                   if ($_SESSION['lang'] == 'eng') {
                                                        echo $lang[99]->eng;
                                                    } else {
                                                        echo $lang[99]->rus;
                                                    }
                                        ?></label>
                                        <input type="text" required="required" style="width:100%;" name='fio' placeholder="<?php 
                                                   if ($_SESSION['lang'] == 'eng') {
                                                        echo $lang[99]->eng;
                                                    } else {
                                                        echo $lang[99]->rus;
                                                    }
                                        ?>" >
                                        <label>Email</label>
                                        <input type="text" style="width:100%;" name='email' placeholder="Email">
                                        <label><?php 
                                                   if ($_SESSION['lang'] == 'eng') {
                                                        echo $lang[45]->eng;
                                                    } else {
                                                        echo $lang[45]->rus;
                                                    }
                                        ?></label>
                                        <input type="text" required="required" style="width:100%;"  name='mob' placeholder="<?php 
                                                   if ($_SESSION['lang'] == 'eng') {
                                                        echo $lang[45]->eng;
                                                    } else {
                                                        echo $lang[45]->rus;
                                                    }
                                        ?>" >
                                        <br><br>
                                            <textarea placeholder='Text' name='text' style="width:100%;"></textarea>
                                        <br><br>

                                        <input type="submit" value="<?php 
                                                   if ($_SESSION['lang'] == 'eng') {
                                                        echo $lang[34]->eng;
                                                    } else {
                                                        echo $lang[34]->rus;
                                                    }
                                        ?>">
                                    </form>
                                </div>
                            </div>   
                            <!-- End Form Detailed --> 
                        </div>
                    </div>
                </div>   
                <!-- End content info - Features--> 

                <!-- End content info - Grey Section-->
                <div class="content_info">
                    <!-- Info Resalt-->
                    <div class="content_resalt paddings-mini tabs-detailed">
                        <div class="container wow fadeInUp">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Nav Tabs-->
                                    <ul class="nav nav-tabs" id="myTab">
                                     
                                       <li class="active">
                                         <a href="#faq" data-toggle="tab"><i class="fa fa-check"></i>Gallery</a>
                                        </li>
                                    </ul>
                                    <!-- End Nav Tabs-->

                                    <div class="tab-content">
                                      
                                        <!-- Tab Theree - faq -->
                                        <div class="tab-pane active" id="faq">
                                                <?php
                                             foreach ($data as $key) {
                                                echo $key->gallery;
                                             }
                                            ?>
                                        </div>
                                        <!-- Tend ab Theree - faq -->
                                    </div>  
                                </div>                 
                            </div>
                        </div>
                    </div>
                    <!-- End Info Resalt-->
                </div>   
                <!-- End content info - Grey Section--> 

                <!-- content info - testimonials-->
            
                <!-- End content info - testimonials-->
            </div>
            <!-- End Content Central -->
      