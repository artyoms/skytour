<?php
 
    if(isset($_SESSION['lang'])) {
        
    } else {
        $_SESSION['lang'] = 'rus';
    } 

?>   <!-- Section Title-->    
            <div class="section-title-02"  style="width: 1230px;    margin: 0 auto;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <h1><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[2]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[2]->rus;
                                    }

                                 ?></h1>
                        </div>
                        <div class="col-md-8">
                            <div class="crumbs">
                                <ul>
                                    <li><a href="/"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[1]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[1]->rus;
                                    }

                                 ?></a></li>
                                    <li>/</li>
                                    <li><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[2]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[2]->rus;
                                    }

                                 ?></li>                                       
                                </ul>    
                            </div>
                        </div>
                    </div>
                </div>  
            </div>   
            <!-- End Section Title-->

            <!--Content Central -->
            <div class="content-central">
                <!-- Shadow Semiboxed -->
            
                <!-- End Shadow Semiboxed -->

                <!-- Google Map --> 
                <iframe style="width:100%;  overflow: hidden;" height="350"  frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/search?q=SkyTour%20Armenia%2C%20%D1%83%D0%BB%D0%B8%D1%86%D0%B0%20%D0%9F%D0%B0%D1%80%D0%BE%D0%BD%D1%8F%D0%BD%2C%20%D0%95%D1%80%D0%B5%D0%B2%D0%B0%D0%BD%2C%20%D0%90%D1%80%D0%BC%D0%B5%D0%BD%D0%B8%D1%8F&key=AIzaSyAHCjX6YeGVbO4qU42e7PHPxB6_wNvOtUI"></iframe>

                <!-- End content info - page Fill with -->
                <div class="content_info">
                    <div class="paddings-mini">
                        <div class="container">
                            <div class="row">
                                <!-- Sidebars -->
                                <div class="col-md-4">
                                    <aside>
                                        <h4><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[41]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[41]->rus;
                                    }

                                 ?>:</h4>
                                        <address> 
                                          <i class="fa fa-map-marker"></i><strong><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[41]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[41]->rus;
                                    }

                                 ?>: </strong><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[42]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[42]->rus;
                                    }

                                 ?><br>
                                          <i class="fa fa-plane"></i><strong><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[83]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[83]->rus;
                                    }

                                 ?> </strong><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[43]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[43]->rus;
                                    }

                                 ?> 
                                          
                                        </address>


                                        <address>
                                          <strong>Emails:</strong><br>
                                          <i class="fa fa-envelope"></i><strong>Email:</strong><a href="mailto:info@skytour.am"> info@skytour.am</a><br>
                                          <i class="fa fa-envelope"></i><strong>Email:</strong><a href="mailto:incoming2@skytour.am"> incoming2@skytour.am</a>
                                        </address>
                                    </aside>

                                    <hr class="tall">
 
                                </div>
                                <!-- End Sidebars -->

                                <div class="col-md-8">
                                    <h4><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[48]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[48]->rus;
                                    }

                                 ?> : </h4>
                                    <p class="lead">
                                        <?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[49]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[49]->rus;
                                    }

                                 ?>
                                        <br><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[50]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[50]->rus;
                                    }

                                 ?>  <br>

                                        <h4><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[44]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[44]->rus;
                                    }

                                 ?></h4>
                                        <i class="fa fa-phone"></i> <abbr title="Phone"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[44]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[44]->rus;
                                    }

                                 ?></abbr>  (+374 10) 53-88-35<br>
                                        <i class="fa fa-phone"></i> <abbr title="Phone"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[115]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[115]->rus;
                                    }

                                 ?>  </abbr>  (+374 91) 53-88-35<br>
                                        <i class="fa fa-phone"></i> <abbr title="Phone"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[45]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[45]->rus;
                                    }

                                 ?> </abbr>  (+374 60) 53-88-35
                                     
                                    </p>
                                    
                                    <div id="result"></div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
                <!-- End content info - page Fill with  --> 
 
            </div>
            <!-- End Content Central -->
       