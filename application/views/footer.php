
            <!-- footer-->
            <footer id="footer" class="footer-v1">
                <div class="container">
                    <div class="row">
                        <!-- Title Footer-->
                        <div class="col-md-5">
                            <iframe style="width:90%;height:300px;"   src="https://www.youtube.com/embed/PALsbMpASBg?list=PLOrp2yVZWTFNOGnxEZZR407sMPLJyBaaq&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <!-- End Title Footer-->

                        <div class="col-md-7">
                            <div class="row">                             
                                <!-- Social Us-->
                                <div class="col-md-3">
                                    <h3> <?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[82]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[82]->rus;
                                    }

                                 ?></h3>
                                    <ul class="social">
                                        <li class="facebook"><span><i class="fa fa-facebook"></i></span><a target="_blank" href="https://www.facebook.com/skytour.am">Facebook</a></li>
                                        <li class="twitter"><span><i class="fa fa-twitter"></i></span><a target="_blank" href="https://twitter.com/skytourarmenia">Twitter</a></li> 
                                        <li class="twitter"><span><i class="fa fa-youtube"></i></span><a target="_blank" href="https://www.youtube.com/channel/UCHMQRCaa2nuDmngIOaQi7Ew">YouTube</a></li> 
 
                                    </ul> 
                                </div>
                                <!-- End Social Us-->
                                
                                <!-- Recent links-->
                                <div class="col-md-5">
                                    <h3> <?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[116]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[116]->rus;
                                    }

                                 ?> </h3>
                                    <ul> 
                                        <li><i class="fa fa-check"></i> <a href="/index.php/Page/armenia/26">Outgoing Tours</a></li>
                                        <li><i class="fa fa-check"></i> <a href="/index.php/Page/armenia/21">Incoming Tours</a></li>
                                        <li><i class="fa fa-check"></i> <a href="#">Airtickets</a></li>                                    
                                    </ul>
                                </div>
                                <!-- End Recent links-->

                                <!-- Contact Us-->
                                <div class="col-md-4">
                                   <h3> <?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[2]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[2]->rus;
                                    }

                                 ?></h3>
                                   <ul class="contact_footer">
                                        <li>
                                            <i class="fa fa-envelope"></i> <a href="#">info@skytour.am</a>
                                        </li>
                                        <li>
                                            <i class="fa fa-headphones"></i> <a href="#">(+374 10) 53-88-35</a>
                                         </li> 
                                    
                                          <li>
                                            <i class="fa fa-headphones"></i> <a href="#">(+374 91) 53-88-35</a>
                                         </li>
                                                <li>
                                            <i class="fa fa-headphones"></i> <a href="#">Viber: (+374 91) 53-88-35</a>
                                         </li>
                                        <li class="location">
                                            <i class="fa fa-home"></i> <a href="#"> <?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[42]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[42]->rus;
                                    }

                                 ?> <?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[43]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[43]->rus;
                                    }

                                 ?>
</a>
                                        </li>                                   
                                    </ul>
                                </div>
                                <!-- Contact Us-->
                            </div>  
                     
                        </div>
                    </div>
                </div>

                <!-- footer Down-->
                <div class="footer-down">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5">
                                <p>&copy; 2015 Skytour . All Rights Reserved.  2013 - 2015</p>
                            </div>
                            <div class="col-md-7">
                                <!-- Nav Footer-->
                                <ul class="nav-footer">
                                    <li>        
                             
                            <a href="/index.php/Home/lang/eng" style="padding-left: 0px;padding-right: 0px;">
                                <img src="http://avtostankoprom.ru/wp-content/themes/avtostankoprom-tpl/img/flag-eng.jpg" style="width:30px;margin-top:0px;">
                            </a>
                                    <a href="/index.php/Home/lang/rus" style="padding-left: 0px;padding-right: 0px;  padding-right: 0px">
                                <img src="http://www.daviscup.com/itf/flags/46/RUS.gif" style="    margin-left: 8px;width:30px;margin-top:0px;">
                             </a>
                              <a href="/index.php/Home/lang/arm" style="padding-left: 0px;padding-right: 0px;  padding-right: 10px">
                                <img src="http://www.fca.am/borrowwisely/img/arm_flag.jpg" style="    margin-left: 8px;width:30px;margin-top:0px;">
                             </a>
                         </li>
                                    <li><a href="/"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[1]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[1]->rus;
                                    }

                                 ?></a> </li>
                                    <li><a href="/index.php/Page/charter"><?php

                                    if (isset($_SESSION['lang'])) {
                                        echo $lang[21]->$_SESSION['lang'];
                                    } else {
                                        echo $lang[21]->rus;
                                    }

                                 ?></a></li>
                                 </ul>
                                <!-- End Nav Footer-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer Down-->
            </footer>      
            <!-- End footer-->
        </div>
        <!-- End layout-->

        <!-- ======================= JQuery libs =========================== -->
        <!-- jQuery local--> 
        <script src="/template/js/jquery.js"></script>  
        <script src="/template/js/jquery-ui.1.10.4.min.js"></script>                
        <!--Nav-->
        <script src="http://positron-it.ru/js/jquery.inputmask.js"></script> 
        <script src="/template/js/nav/jquery.sticky.js" type="text/javascript"></script>    
        <!--Totop-->
        <script type="text/javascript" src="/template/js/totop/jquery.ui.totop.js" ></script>  
         <!--Accorodion-->
        <script type="text/javascript" src="/template/js/accordion/accordion.js" ></script>  
        <!--Slide Revolution-->
        <script type="text/javascript" src="/template/js/rs-plugin/js/jquery.themepunch.tools.min.js" ></script>      
        <script type='text/javascript' src='/template/js/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>    
        <!-- Maps -->
        <script src="/template/js/maps/gmap3.js"></script>            
        <!--Ligbox--> 
        <script type="text/javascript" src="/template/js/fancybox/jquery.fancybox.js"></script> 
        <!-- carousel.js-->
        <script src="/template/js/carousel/carousel.js"></script>
        <!-- Filter -->
        <script src="/template/js/filters/jquery.isotope.js" type="text/javascript"></script>
        <!-- Twitter Feed-->
        <script src="/template/js/twitter/jquery.tweet.js"></script> 
        <!-- flickr Feed-->
        <script src="/template/js/flickr/jflickrfeed.min.js"></script>    
        <!--Theme Options-->
        <script type="text/javascript" src="/template/js/theme-options/theme-options.js"></script>
        <script type="text/javascript" src="/template/js/theme-options/jquery.cookies.js"></script> 
        <!-- Bootstrap.js-->
        <script type="text/javascript" src="/template/js/bootstrap/bootstrap.js"></script> 
        <script type="text/javascript" src="/template/js/bootstrap/bootstrap-slider.js"></script> 
        <!--MAIN FUNCTIONS-->
        <script type="text/javascript" src="/template/js/main.js"></script>
        <!-- ======================= End JQuery libs =========================== -->

        <!--Slider Function-->
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('.tp-banner').show().revolution({
                    dottedOverlay:"none",
                    delay:5000,
                    startwidth:1170,
                    startheight:700,
                    minHeight:500,
                    navigationType:"none",
                    navigationArrows:"solo",
                    navigationStyle:"preview1"
                });             
            }); //ready
        </script>
 <script>
if (window.screen.width > 800) {
    (function(){
    var widget_id = 779329;
    _shcp =[{widget_id : widget_id}];
    var lang =(navigator.language || navigator.systemLanguage 
    || navigator.userLanguage ||"en")
    .substr(0,2).toLowerCase();
    var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/"+ lang +"/widget.js";
    var hcc = document.createElement("script");
    hcc.type ="text/javascript";
    hcc.async =true;
    hcc.src =("https:"== document.location.protocol ?"https":"http")
    +"://"+ url;
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(hcc, s.nextSibling);
    })();
};
</script>

        <script type="text/javascript">
$(document).ready(function() {
    if($("#number")){
        $("#number").inputmask("0(99)99-99-99");//маска
    }
    // chaeck !document.getElementById("number").value.match(/^0\([0-9]{2}\)[0-9]{2}-[0-9]{2}\-[0-9]{2}/);

});

    function form_check(){
        if (!document.getElementById("number").value.match(/^0\([0-9]{2}\)[0-9]{2}-[0-9]{2}\-[0-9]{2}/)) {

            alert("Incorrect Mobile Number");
            return false;
        }else {

            return true;
        }
    }
    
    if ($( "#country" )) {
        $('#getcount').on('change', function() { 
            $.get("/index.php/Page/chart/"+document.getElementById('getcount').value, function( data ) {
              $( ".counts" ).html( data ); 
            });
        });
    };
</script>

<!-- End SiteHeart code -->
    </body>
</html>