<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>SkyTour | Login</title>

	<link rel="apple-touch-icon" sizes="144x144" href="/application/views/admin/apple-touch-icon-ipad-retina.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="/application/views/admin/apple-touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="/application/views/admin/apple-touch-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="/application/views/admin/apple-touch-icon-iphone.png" />
	<link rel="shortcut icon" type="image/x-icon" href="/application/views/admin/favicon.ico" />

	<!-- bootstrap -->
    <link href="/application/views/admin/css/bootstrap/bootstrap.css" rel="stylesheet" />
    
    <link rel="stylesheet" href="/application/views/admin/css/font-awesome-4.0.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="/application/views/admin/css/jquery-ui.css" />
    

	<link href="/application/views/admin/css/style.css" rel="stylesheet" type="text/css" />

	
</head>
<body>
	<div id="loading">
		<div>
			<div></div>
		    <div></div>
		    <div></div>
		</div>
	</div>
	<div id="wrapper" class="login">

		<div id="top">
			<div id="topBar" class="clearfix widget-content pad10">
					<a class="logo" href="index.html">
						<img src="/template/img/logo/logo.png" style="width:100px;" rel="logo">
					</a>
			 	
			</div> <!-- /topBar -->
			
		</div> <!-- /top -->

	<div class="widget-content pad20 clearfix">
			<div class="userImg">
			<img src="http://cometoarmenia.am/tour_operators/2698_15098_592054924170676_1179059370_n.jpg" rel="user">
			</div>
			<h3 class="center">SkyTour Armenia</h3>
			<span class="center light">Вход</span>
				<form action="/index.php/Admin/auth" method="post">
					<input type="text" class="input-text" name="username" placeholder="Логин" />
					<input type="password" class="input-text" name="password" placeholder="Пароль" />
					<span class="custom-input center">
		                <button type="submit" class="btn btn-blue btn-full">Войти</button> 
		            </span>
		        </form>
	</div>	<!-- /widget-content -->	


		</div>  <!-- /wrapper -->

	<div class="clearfix"></div>
	 

	<script type="text/javascript" src="/application/views/admin/js/prefixfree.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery.sparkline.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery.easytabs.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/excanvas.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery.flot.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery.flot.resize.js"></script>
	
	
	<script type="text/javascript" src="/application/views/admin/js/main.js"></script>

	
</body>
</html>