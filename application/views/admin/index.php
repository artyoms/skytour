<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sky Admin</title>

	<link rel="apple-touch-icon" sizes="144x144" href="/application/views/admin/apple-touch-icon-ipad-retina.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="/application/views/admin/apple-touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="/application/views/admin/apple-touch-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="/application/views/admin/apple-touch-icon-iphone.png" />
	<link rel="shortcut icon" type="image/x-icon" href="/application/views/admin/favicon.ico" />

	<!-- bootstrap -->
    <link href="/application/views/admin/css/bootstrap/bootstrap.css" rel="stylesheet" />
    
    <link rel="stylesheet" href="/application/views/admin/css/font-awesome-4.0.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="/application/views/admin/css/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="/application/views/admin/css/toastr.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href="/application/views/admin/css/style.css" rel="stylesheet" type="text/css" />

	
</head>
<body>
	<div id="loading">
		<div>
			<div></div>
		    <div></div>
		    <div></div>
		</div>
	</div>
	<div id="wrapper" class="container">

		<div id="top">
			<div id="topBar">
				<div class="wrapper20">
				 
					<div class="topNav clearfix"> 
						<ul class="tNav clearfix">
							 <li>
								<a data-toggle="dropdown" href="index.html#" class="dropdown-toggle"><i class="fa fa-gear icon-white"></i></a>
								  <ul class="dropdown-menu pull-right">
								    <li><a href="index.html#"><i class="fa fa-pencil"></i> Настройки</a></li> 
	                                <li><a href="index.html#"><i class="fa fa-ban"></i> Заблокировать</a></li>
	                                <li class="divider"></li>
	                                <li><a href="index.html#"> Другое</a></li>
								  </ul>
							</li>
						 
							<li><a href="/index.php/Admin/out"><i class="fa fa-times icon-white"></i></a></li>
						</ul>
					</div> <!-- /topNav -->
				</div>
			</div> <!-- /topBar -->

			<div id="profile">
				<div class="wrapper20">
					<div class="userInfo">
						<div class="userImg">
							<img src="http://cometoarmenia.am/tour_operators/2698_15098_592054924170676_1179059370_n.jpg" rel="user">
						</div>
						<div class="userTxt">
							<span class="fullname">SkyTour.am</span> <i class="fa fa-chevron-right"></i><br>
							<span class="username">@skytour</span>
						</div>
					</div> <!-- /userInfo -->
					<div class="userStats">
						<div class="uStat">
							<div class="stat-name">
								Посетители <div class="stat-badge">+14</div>
							</div>
							<div class="stat-number">218k</div>
						</div>
						<div class="uStat">
							<div class="stat-name">
								Просмотры <div class="stat-badge">+27</div>
							</div>
							<div class="stat-number">1857</div>
						</div>
				 
						 
					</div>

					<i class="fa fa-bars icon-nav-mobile"></i>

				</div>
			</div> <!-- /profile -->
		</div> <!-- /top -->

		<div id="sidebar">
			<ul class="mainNav">
				<li class="active">
					<a href="index.html#"><i class="fa fa-user"></i><br>Главное</a>
				</li>
				<!--<li>
					<a href="ui.html"><i class="fa fa-paperclip"></i><br>Чартеры</a>
				</li>
				<li>
					<a href="form.html"><i class="fa fa-tasks"></i><br>Армения</a>
				</li> 
				<li>
					<a href="grid.html"><i class="fa fa-th"></i><br>Страницы</a>
				</li>-->
			 
			</ul>
		</div> <!-- /sidebar -->

		<div id="main" class="clearfix">
			<div class="topTabs">
					
					<div id="topTabs-container-home">
						<div class="topTabs-header clearfix">
	
						<div class="secInfo sec-dashboard">
							<h1 class="secTitle">Главная</h1>
							<span class="secExtra">Добро пожаловать</span>
						</div> <!-- /SecInfo -->
						
						<ul class="etabs tabs">
							<li class="tab">
								<a href="index.html#tab1">
									<span class="to-hide">
										<i class="fa fa-th"></i><br>Управление базой
									</span>
									<i class="fa icon-hidden fa-th ttip" data-ttip="Quick Menu"></i>
								</a>
							</li>
							<li class="tab">
								<a href="index.html#tab2">
									<span class="to-hide">
										<i class="fa fa-bar-chart-o"></i><br>Страницы сайта
									</span>
									<i class="fa icon-hidden fa-bar-chart-o ttip" data-ttip="Site Statistics"></i>
								</a>
							</li>
							<li class="tab">
								<a href="index.html#tab3">
									<span class="to-hide">
										<i class="fa fa-calendar"></i><br>Календарь
									</span>
									<i class="fa icon-hidden fa-calendar ttip" data-ttip="Calendar"></i>
								</a>
							</li>
						</ul> <!-- /tabs -->
						</div><!-- /topTabs-header -->

						<div class="topTabsContent">
								<div id="tab1">
									<a href="index.html#" class="hexagon red ttip" data-ttip="Чартеры"><i class="fa fa-plane"></i></a>
									<a href="index.html#" class="hexagon aqua ttip" data-ttip="Гостиницы"><i class="fa fa-bed"></i></a>
									<a href="index.html#" class="hexagon grey ttip" data-ttip="Армения"><i class="fa fa-caret-up"></i></a>
									<a href="index.html#" class="hexagon orange ttip" data-ttip="Гостиницы Армения"><i class="fa fa-caret-up"></i><i class="fa fa-bed"></i></a>
									<a href="index.html#" class="hexagon blue ttip" data-ttip="Страницы сайта"><i class="fa fa-list-alt"></i></a>
									<a href="index.html#" class="hexagon grey ttip" data-ttip="Слайдеры"><i class="fa fa-picture-o"></i></a>
								</div>
								<div id="tab2">
								    <a href="index.html#" class="hexagon red ttip" data-ttip="Иконки Главная"><i class="fa fa-fonticons"></i></a>
									<a href="index.html#" class="hexagon aqua ttip" data-ttip="СлайдБар Главная"><i class="fa fa-file-text-o"></i></a>
									<a href="index.html#" class="hexagon grey ttip" data-ttip="Армения История"><i class="fa fa-history"></i></a>
									<a href="index.html#" class="hexagon orange ttip" data-ttip="Армения Ереван"><i class="fa fa-file-code-o"></i></a>
									<a href="index.html#" class="hexagon blue ttip" data-ttip="Достопримечательности Армении"><i class="fa fa-university"></i></a>
									<a href="index.html#" class="hexagon grey ttip" data-ttip="Армения Климат"><i class="fa fa-line-chart"></i></a>

								    <a href="index.html#" class="hexagon grey ttip" data-ttip="Религия Армении"><i class="fa fa-book"></i></a>
									<a href="index.html#" class="hexagon blue ttip" data-ttip="Культура Армении"><i class="fa fa-users"></i></a>
									<a href="index.html#" class="hexagon orange ttip" data-ttip="Классические туры"><i class="fa fa-plus-square "></i></a>
									<a href="index.html#" class="hexagon grey ttip" data-ttip="Историка- культурный тур"><i class="fa fa-bus"></i></a>
									<a href="index.html#" class="hexagon aqua ttip" data-ttip="Походы пешком"><i class="fa fa-user-plus"></i></a>
									<a href="index.html#" class="hexagon red ttip" data-ttip="Индивидуальные Туры"><i class="fa fa-rocket"></i></a>

								    <a href="index.html#" class="hexagon red ttip" data-ttip="Трансфер с Аэропорта"><i class="fa fa-building-o"></i></a>
									<a href="index.html#" class="hexagon aqua ttip" data-ttip="Бронирование гостиницы"><i class="fa fa-hotel"></i></a>
									<a href="index.html#" class="hexagon grey ttip" data-ttip="Организация конференции"><i class="fa fa-user-secret"></i></a>
									<a href="index.html#" class="hexagon orange ttip" data-ttip="Аренда машины с водителем"><i class="fa fa-car"></i></a>
								  


								</div>
								<div id="tab3">
								    <div class="small-calendar cal-tab"></div>
								</div>
						</div> <!-- /topTabContent -->

					</div> <!-- /tab-container -->

				<!-- </div> -->
			</div> <!-- /topTabs -->
			
			<div class="divider"></div>
			
			<div class="fluid">
				
				<div class="widget leftcontent grid12">
					<div class="widget-header">
						<div class="widget-controls">
	  						<div class="btn-group xtra"> <!-- btn dd -->
								<a href="index.html#" onclick="return false;" class="icon-button dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i></a>
								<ul class="dropdown-menu pull-right">
	                                <li><a href="index.html#"><i class="fa fa-pencil"></i> Edit</a></li>
	                                <li><a href="index.html#"><i class="fa fa-trash-o"></i> Delete</a></li>
	                                <li><a href="index.html#"><i class="fa fa-ban"></i> Ban</a></li>
	                                <li class="divider"></li>
	                                <li><a href="index.html#"> Other actions</a></li>
	                            </ul>
                            </div><!-- /btn dd -->
						</div>
					</div>
					<div id="statsTabs-container">
						<ul class="etabs stats-tabs">
							<li class="tab"><a href="index.html#stat-tab1">Редактор</a></li> 
						</ul>
						<div class="statsTabsContent">

							<div id="stat-tab1">
								  <center><h4>Основные настройки</h4></center>
							</div>
 
						</div>
					</div>

					<div class="divider"></div>

				</div> <!-- /widget -->
				
			</div> <!-- /fluid -->

		 
			</div> <!-- /main -->

		</div> <!-- /wrapper -->

	
	<div class="clearfix"></div>
	 
	<script type="text/javascript" src="/application/views/admin/js/prefixfree.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery.sparkline.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery.hashchange.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery.easytabs.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/excanvas.min.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery.flot.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/jquery.flot.resize.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/toastr.min.js"></script>

	<script type="text/javascript" src="/application/views/admin/js/main.js"></script>
	<script type="text/javascript" src="/application/views/admin/js/charts.js"></script>
	<script type="text/javascript">
		toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "positionClass": "toast-top-right",
		  "onclick": null,
		  "showDuration": "300",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}
		setTimeout(function(){
			toastr.info('<span style="color:#333;">Добро пожаловать в Панель Управления SkyTour.</span>');	
		},2000) ;
		
		setTimeout(function(){
			toastr.warning('<span style="color:#333;">У вас есть ошибка...</span>');	
		},4500) ;


	</script>
	
</body>
</html>