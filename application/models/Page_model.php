<?php if( ! defined('BASEPATH')) exit ('No direct bla bla bla');
	class Page_model extends CI_Model 
	{
		 function get_top_charters()
		 {	
		 	$this->db->where('top','1');
		 	$query = $this->db->get('charter');
		 	return $query->result();
		 }

		 function get_hotels(){
		 	$this->db->limit(5);
		 	//$this->db->get('hotels');
		 	$this->db->select('*');
		 	$this->db->order_by('id', 'RANDOM');
		 	$this->db->from('hotels');
		 	$this->db->join('charter', 'charter.chid = hotels.charterid', 'left');

		 	$this->db->join('countries', 'countries.countid = charter.countrie', 'left');
		 	$query = $this->db->get();
		 	return $query->result();
		 }


		 function get_char($count){
		 	$this->db->select('*');
		 	$this->db->from('charter');
		 	$this->db->where('countrie',$count);
		 	$query = $this->db->get();
			$res =  $query->result();

		 	if (isset($_SESSION['lang'])) {
		 		$lang = $_SESSION['lang'];
		 	} else{
		 		$lang = 'rus';
		 	}

		  
			 return $res;
		 }	
		
		public function get_trans(){ 
			$query = $this->db->get('transport');

			return $query->result();
		}

		public function get_armhote_county(){
			$this->db->select("*");
			$this->db->from('categories_hotel');
			$query = $this->db->get();

		 	$res =  $query->result();
		 	
		 	if (isset($_SESSION['lang'])) {
		 		$lang = $_SESSION['lang'];
		 	} else{
		 		$lang = 'rus';
		 	}

		 	$str = "name_".$lang;

		 	for ($i=0; $i < count($res); $i++) { 
		 	
		 		$get = $res[$i]->{$str}; 

		 		$res[$i]->name_rus  = $get;
		 		$res[$i]->name_arm  = $get;
		 		$res[$i]->name_eng  = $get;
		 	}		 	


 			return $res;
		}


		 function get_hotel_search($id){

		 	$this->db->select('*');
		 	$this->db->from('hotels');
 
		 	$this->db->where('hotels.charterid', $id);
		// 	$this->db->where('data_end','data_end`) => date(`'.$date_two.'`)');
		  
		 	$this->db->join('charter', 'charter.chid = hotels.charterid');
 
		 	$this->db->join('countries', 'countries.countid = charter.countrie');
		 	
		 	$query = $this->db->get();
		  
		 	return $query->result();

		 }


		 function get_armHotelsRand($id = NULL){
			if ($id == NULL) {
				# code...
				
			 	$this->db->limit(5);
			 	$this->db->select('*');
			 	$this->db->from('base_hotel');
			 	$this->db->order_by('id', 'RANDOM');
			 	$query = $this->db->get();
$res =  $query->result();

		 	if (isset($_SESSION['lang'])) {
		 		$lang = $_SESSION['lang'];
		 	} else{
		 		$lang = 'rus';
		 	}

		 	$str = "title_".$lang;

		 	$desc = "desctiption_".$lang;


		 	for ($i=0; $i < count($res); $i++) { 
		 	
		 		$get = $res[$i]->{$str};
		 		$de = $res[$i]->{$desc};

		 		$res[$i]->title_rus  = $get;
		 		$res[$i]->title_arm  = $get;
		 		$res[$i]->title_eng  = $get;
				
				$res[$i]->desctiption_arm  = $de;
				$res[$i]->desctiption_arm  = $de;
				$res[$i]->desctiption_arm  = $de;
		 	}
			 		 
			 return $res;
			 } else {
			 	$this->db->select('*');
			 	$this->db->where('categid', $id);
			 	$this->db->from('base_hotel'); 
			 	$query = $this->db->get();
$res =  $query->result();

		 	if (isset($_SESSION['lang'])) {
		 		$lang = $_SESSION['lang'];
		 	} else{
		 		$lang = 'rus';
		 	}

		 	$str = "title_".$lang;

		 	$desc = "desctiption_".$lang;


		 	for ($i=0; $i < count($res); $i++) { 
		 	
		 		$get = $res[$i]->{$str};
		 		$de = $res[$i]->{$desc};

		 		$res[$i]->title_rus  = $get;
		 		$res[$i]->title_arm  = $get;
		 		$res[$i]->title_eng  = $get;
				
				$res[$i]->desctiption_arm  = $de;
				$res[$i]->desctiption_arm  = $de;
				$res[$i]->desctiption_arm  = $de;
		 	}
			 		 
			 return $res;
			 }
		 }

		 function get_hotels_search($date_one,$date_two,$count,$cash){

		 	$this->db->select('*');
		 	$this->db->from('hotels');

		 	$this->db->where('date(`data_to`) >=',' '.$date_one.'');
		 	$this->db->where('date(`data_end`) <=',' '.$date_two.'');
		 	$this->db->where('charter.countrie', $count);
		// 	$this->db->where('data_end','data_end`) => date(`'.$date_two.'`)');
		 	
		 	if ($cash != '') {
		 	
		 		$result = explode(',', $cash);

		 		$this->db->where('cash >=', $result[0]);
		 		$this->db->where('cash <=', $result[1]);
		 	}

		 	$this->db->join('charter', 'charter.chid = hotels.charterid');

		 	$this->db->join('countries', 'countries.countid = charter.countrie');
		 	$query = $this->db->get();
		 	return $query->result();
		 }

		 function get_hotel($id){
		 	$this->db->where('hotels.id',$id);

	 	//$this->db->get('hotels');
		 	$this->db->select('*');
		 	$this->db->from('hotels');
		 	$this->db->join('charter', 'charter.chid = hotels.charterid');
		 
		 	$this->db->join('countries', 'countries.countid = charter.countrie');
		 	$query = $this->db->get();


		 	//$query = $this->db->get('hotels');
		 	return $query->result();
		 }
		function get_armhotel($id){
		 	$this->db->where('id',$id);

	 	//$this->db->get('hotels');
		 	$this->db->select('*');
		 	$this->db->from('base_hotel');
		  	$query = $this->db->get();
			

			$res =  $query->result();

		 	if (isset($_SESSION['lang'])) {
		 		$lang = $_SESSION['lang'];
		 	} else{
		 		$lang = 'rus';
		 	}

		 	$str = "title_".$lang;

		 	$desc = "desctiption_".$lang;


		 	for ($i=0; $i < count($res); $i++) { 
		 	
		 		$get = $res[$i]->{$str};
		 		$de = $res[$i]->{$desc};

		 		$res[$i]->title_rus  = $get;
		 		$res[$i]->title_arm  = $get;
		 		$res[$i]->title_eng  = $get;
				
				$res[$i]->desctiption_arm  = $de;
				$res[$i]->desctiption_arm  = $de;
				$res[$i]->desctiption_arm  = $de;
		 	}
			 		 
			 return $res;

		 }

		 function get_armenia($id){
		 	$this->db->where('id',$id);
		 	$query = $this->db->get('vizit');
		 	$res =  $query->result();

		 	if (isset($_SESSION['lang'])) {
		 		$lang = $_SESSION['lang'];
		 	} else{
		 		$lang = 'rus';
		 	}

		 	$str = "name_".$lang;

		 	$desc = "description_".$lang;


		 	for ($i=0; $i < count($res); $i++) { 
		 	
		 		$get = $res[$i]->{$str};
		 		$de = $res[$i]->{$desc};

		 		$res[$i]->name_rus  = $get;
		 		$res[$i]->name_arm  = $get;
		 		$res[$i]->name_eng  = $get;
				
				$res[$i]->description_eng  = $de;
				$res[$i]->description_rus  = $de;
				$res[$i]->description_arm  = $de;
		 	}
			 		 
			 return $res;
		 }

		 function get_coutry(){
		 	$query = $this->db->get('countries');

		 	$res =  $query->result();

		 	if (isset($_SESSION['lang'])) {
		 		$lang = $_SESSION['lang'];
		 	} else{
		 		$lang = 'rus';
		 	}

		 	$str = "name_".$lang;

		 	for ($i=0; $i < count($res); $i++) { 
		 	
		 		$get = $res[$i]->{$str}; 

		 		$res[$i]->name_rus  = $get;
		 		$res[$i]->name_arm  = $get;
		 		$res[$i]->name_eng  = $get;
		 	}		 	



		 	return $res;
		 }

  
	}
 